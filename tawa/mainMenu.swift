//
//  mainMenu.swift
//  tawa
//
//  Created by Cristopher Nunez Del Prado on 26/08/17.
//  Copyright © 2017 Cristopher Nunez Del Prado. All rights reserved.
//

import UIKit
import GoogleMobileAds


class mainMenu: UIViewController {
    @IBOutlet var startGameLabel: UILabel!

    @IBOutlet weak var GoogleBannerView: GADBannerView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if Reachability.isConnectedToNetwork() == true
        {
            GoogleBannerView.adUnitID="ca-app-pub-1462291695436824/7924747470"
            GoogleBannerView.rootViewController=self
            GoogleBannerView.load(GADRequest())
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func openItunes(_ sender: Any) {
        //UIApplication.shared.openURL(NSURL(string: "itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4")! as URL)
        //UIApplication.shared.openURL(NSURL(string: "itms-apps://itunes.apple.com/app/bars/id706081574")! as URL)
        //UIApplication.shared.openURL(NSURL(string: "https://itunes.apple.com/pe/developer/cristopher-nunez-del-prado/id1209583530")! as URL)
        UIApplication.shared.openURL(NSURL(string: "itms-apps://itunes.apple.com/pe/developer/cristopher-nunez-del-prado/id1209583530")! as URL)
    }
    
    @IBAction func openFacebook(_ sender: Any) {
        let facebookURL = NSURL(string: "fb://profile/134145790574321")!
        if UIApplication.shared.canOpenURL(facebookURL as URL) {
            UIApplication.shared.openURL(facebookURL as URL)
        }
        else {
            UIApplication.shared.openURL(NSURL(string: "https://www.facebook.com/Bit-Room-134145790574321")! as URL)
        }
        
    }
    
}
