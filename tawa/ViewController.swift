//
//  ViewController.swift
//  tawa
//
//  Created by Cristopher Nunez Del Prado on 29/06/17.
//  Copyright © 2017 Cristopher Nunez Del Prado. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation
import Firebase

extension UIView {
    /*func rotate360Degrees(duration: CFTimeInterval = 3) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount=Float.infinity
        self.layer.add(rotateAnimation, forKey: nil)
    }*/
    func rotate360Degrees(duration: CFTimeInterval = 0.5, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * 2.0)
        rotateAnimation.duration = duration
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as! CAAnimationDelegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
}

class ViewController: UIViewController {
    
    var interstitial: GADInterstitial!
    
    var audioPlayer = AVAudioPlayer()
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var gainPointsLabel: UILabel!
    @IBOutlet weak var timeWheelImage: UIImageView!
    @IBOutlet var mainView: UIView!
    
    
    
    @IBOutlet var timerImage: UIImageView!
    @IBOutlet var lookIcon: UIImageView!
    @IBOutlet weak var plusLifeImage: UIImageView!
    
    
    @IBOutlet var menuButton: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    
    var cb1 = customButton()
    var cb2 = customButton()
    var cb3 = customButton()
    var cb4 = customButton()
    
    @IBOutlet var image1: UIImageView!
    @IBOutlet var image2: UIImageView!
    @IBOutlet var image3: UIImageView!
    @IBOutlet var image4: UIImageView!
    
    @IBOutlet weak var controlView: UIView!
    @IBOutlet var timeLeft: UILabel!
    @IBOutlet var lifesLabel: UILabel!
    var rect = CGRect()
    var selectedPath  = [Int]()
    var allPathsArray = [[Int]]()
    var onGamePath = [Int]()
    var currentLevel = 1
    var lifes=2
    var highScore=0
    var waitTime=1
    var score=0
    var gainedScore=0
    var currentPosition=1
    var hasStarted=false
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.scoreLabel.text="00"
        self.gainPointsLabel.text=""
        self.saveHighScoreFirstTime()
        
        self.setDiamondPosition(position: 1)
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: cb1.sound! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
        }
        catch{
            print(error)
        }
        audioPlayer.play()

        audioPlayer.stop()
        
        //----Ads
        if Reachability.isConnectedToNetwork() == true
        {
            self.interstitial = self.createAndLoadAd()
        }
        
        
        //----
        
    }
    override func viewDidAppear(_ animated: Bool) {
        createPaths()
        chooseOnGamePath()

    }
    func startGame(){
        self.timerImage.isHidden=false
        showLifes()
        self.initialPresentation(index: 0)
        
        /*let when = DispatchTime.now() + 1.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.setDiamondPosition(position: self.currentPosition)
        }*/
        

        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //BUTTONS
    @IBAction func pb1(_ sender: Any) {
        if self.hasStarted == false{
            self.image1.backgroundColor=self.cb1.sColor
            self.makeSound(key: self.cb1.buttonValue)
            let when = DispatchTime.now() + 0.3
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.image1.backgroundColor=self.cb1.nColor
                
            }
        }
        else{
            self.pauseChrono()
            //self.image1.image=UIImage(named:"b1S.png")
            self.image1.backgroundColor=self.cb1.sColor
            self.makeSound(key: self.cb1.buttonValue)
            self.lockButtons()
            let when = DispatchTime.now() + 0.3
            self.appendItem(item: self.cb1.buttonValue)
            self.tappedButton()
            DispatchQueue.main.asyncAfter(deadline: when) {
                //self.image1.image=UIImage(named:"b1N.png")
                self.image1.backgroundColor=self.cb1.nColor
                
            }
        }
        
        
    }
    @IBAction func pb2(_ sender: Any) {
        
        if self.hasStarted == false{
            self.image2.backgroundColor=self.cb2.sColor
            self.makeSound(key: self.cb2.buttonValue)
            let when = DispatchTime.now() + 0.3
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.image2.backgroundColor=self.cb2.nColor
                
            }
        }
        else{
        
            self.pauseChrono()
            //self.image2.image=UIImage(named:"b2S.png")
            self.image2.backgroundColor=self.cb2.sColor
            self.lockButtons()
            self.makeSound(key: self.cb2.buttonValue)
            let when = DispatchTime.now() + 0.3
            self.appendItem(item: self.cb2.buttonValue)
            self.tappedButton()
            DispatchQueue.main.asyncAfter(deadline: when) {
                
                //self.image2.image=UIImage(named:"b2N.png")
                self.image2.backgroundColor=self.cb2.nColor
            }
        }
        
    }
    @IBAction func pb3(_ sender: Any) {
        
        if self.hasStarted == false{
            self.image3.backgroundColor=self.cb3.sColor
            self.makeSound(key: self.cb3.buttonValue)
            let when = DispatchTime.now() + 0.3
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.image3.backgroundColor=self.cb3.nColor
                
            }
        }
        else{
            self.pauseChrono()
            //self.image3.image=UIImage(named:"b3S.png")
            self.image3.backgroundColor=self.cb3.sColor
            self.lockButtons()
            self.makeSound(key: self.cb3.buttonValue)
            let when = DispatchTime.now() + 0.3
            self.appendItem(item: self.cb3.buttonValue)
            self.tappedButton()
            DispatchQueue.main.asyncAfter(deadline: when) {
                
                //self.image3.image=UIImage(named:"b3N.png")
                self.image3.backgroundColor=self.cb3.nColor
            }
        }
        
    }
    @IBAction func pb4(_ sender: Any) {
        
        if self.hasStarted == false{
            self.image4.backgroundColor=self.cb4.sColor
            self.makeSound(key: self.cb4.buttonValue)
            let when = DispatchTime.now() + 0.3
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.image4.backgroundColor=self.cb4.nColor
                
            }
        }
        else{
            self.pauseChrono()
            //self.image4.image=UIImage(named:"b4S.png")
            self.image4.backgroundColor=self.cb4.sColor
            self.lockButtons()
            self.makeSound(key: self.cb4 .buttonValue)
            let when = DispatchTime.now() + 0.3
            self.appendItem(item: self.cb4.buttonValue)
            self.tappedButton()
            DispatchQueue.main.asyncAfter(deadline: when) {
                
                //self.image4.image=UIImage(named:"b4N.png")
                self.image4.backgroundColor=self.cb4.nColor
            }
        }
        
        
    }
    
    func lockButtons(){
        self.button1.isEnabled=false
        self.button2.isEnabled=false
        self.button3.isEnabled=false
        self.button4.isEnabled=false
        self.menuButton.isEnabled=false
        
    
    }
    func unlockButtons(){
        self.button1.isEnabled=true
        self.button2.isEnabled=true
        self.button3.isEnabled=true
        self.button4.isEnabled=true
        self.menuButton.isEnabled=true
        
        
        
    }
    
    
    
    //PATH
    func appendItem(item:Int){
        self.onGamePath.append(item)
    }
    func cleanOnGamePath(){
        self.onGamePath.removeAll()
    }
    func isPathCorrect()->Bool{
        var currentPathFromSelected = [Int]()
        //for i in 0...(currentLevel-1) {
        for i in 0...(onGamePath.count-1) {
            currentPathFromSelected.append(selectedPath[i])
        }
        if onGamePath == currentPathFromSelected{
            return true
        }
        else{
            return false
        }
        return true
    }
    func isCorrectWholePath()->Bool{
        if self.currentLevel==onGamePath.count{
            return true
        }
        return false
    }
    
    
    //LIFES
    func isAlive()->Bool{
        if self.lifes>0{
            return true
        }
        else{
            return false
        }
    }
    func getMoreLifes(){
        if(currentLevel>=5 && currentLevel%5==0){
            lifes=lifes+1
            showLifes()
            self.plusLifeImage.isHidden=false
            var when = DispatchTime.now() + 0.8
            DispatchQueue.main.asyncAfter(deadline: when) {
                    self.plusLifeImage.isHidden=true
            }
            selectDiamonPosition()
        }
    }
    func loseLife(){
        self.lifes-=1
        showLifes()
        loseLifeAnimation()
    }
    
    
    //CURRENTLEVEL
    func cleanCurrentLevel(){
        self.currentLevel=0
    }
    func improveCurrentLevel(){
        self.currentLevel+=1
    }
    
    //HIGHSCORE
    func isHighScore()->Bool{
        var localHS = defaults.integer(forKey: "HS")
        
            if localHS < self.score{
                return true
            }
        
        return false
    }
    func setHighScore(){
        self.highScore=self.score
        defaults.set(self.highScore, forKey: "HS")
    }
    func getHighScore()->Int{
        var HS=self.defaults.integer(forKey: "HS")
        return HS
    }
    func showAlertHighScore(){
        let  alert = UIAlertController(title: NSLocalizedString("Congratulations_Text", comment: ""), message: "\(NSLocalizedString("New_Highscore_Text", comment: "")) \(self.highScore)", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Continue_Game_Text", comment: ""), style: UIAlertActionStyle.default, handler: { action in self.gameOver()}))
        
        self.present(alert, animated: true, completion: nil)
    }
    func saveHighScoreFirstTime(){
        var localHS = self.defaults.integer(forKey: "HS")
        if localHS == nil{
            self.defaults.set(0, forKey: "HS")
        }
    }
    
    //CHRONO
    
    var seconds:Double = 4{
        didSet {
            if seconds != 4{
                /*var fractionalProgress = Float(seconds) / 100.0
                fractionalProgress = (fractionalProgress+1)*20
                fractionalProgress = (fractionalProgress-20)
                //fractionalProgress = (10-(2*fractionalProgress))*10.0
                let animated = seconds != 0
                print("progreso... \(fractionalProgress)")
                
                UIView.animate(withDuration: 0.8, animations: { () -> Void in
                    //self.progressView.setProgress(fractionalProgress, animated: true)
                })*/
                
                
                self.setTimerImage()
                
                
            }
            else{
                
                self.timerImage.image=nil
                
                
                //self.progressView.setProgress(1.0, animated: true)
                

            }
            //self.timeLeft.text=String(self.seconds)
            
            
            
        }
    }
    var timer = Timer()
    var isTimerRunning = false
    var resumeTapped = false
    
    func startChrono(){
        if timer.isValid == false{
            runTimer()
            self.setTimerImage()
        }
        
    }
    func runTimer(){
        timer = Timer.scheduledTimer(timeInterval:1.0, target:self, selector: (#selector(ViewController.updateTimer)),userInfo:nil, repeats: true)
    }
    func updateTimer(){
        
        if self.seconds>=0{
            if self.seconds==1{
                self.timerImage.image=UIImage(named: "timer_0.png")
                self.lockButtons()
                pauseChrono()
                self.loseLife()
                
                let when = DispatchTime.now() + 1.0
                DispatchQueue.main.asyncAfter(deadline: when) {
                    if self.isAlive(){
                        self.animateCurrentPath(index:0)
                        //seconds=5
                    }
                    else{
                        self.gameOver()
                    }
                }
                
            }
            else
            {
                seconds-=1
                
            }

        }
        else{
            self.pauseChrono()
        }
        
        
    }
    func pauseChrono(){
        timer.invalidate()
    }
    func resetChrono(){
        timer.invalidate()
        seconds=4
        startChrono()
    }
    
    
    
    //SHOW
    
    
    //GAME
    
    @IBAction func stopGame(_ sender: Any) {
        
        if hasStarted==false{
            hasStarted=true
            startGame()
            self.menuButton.setBackgroundImage(UIImage(named: "pause_button"), for: UIControlState.normal)
        }
        else{
            pauseChrono()
            let  alert = UIAlertController(title: NSLocalizedString("Pause_Game_Text", comment: ""), message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Continue_Game_Text", comment: ""), style: UIAlertActionStyle.default, handler: { action in self.startChrono()}))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Restart_Game_Text", comment: ""), style: UIAlertActionStyle.default, handler: { action in self.confirmAction(action: 1)}))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Finish_Game_Text", comment: ""), style: UIAlertActionStyle.default, handler: { action in self.confirmAction(action: 2)}))
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func confirmAction(action:Int){
        if action==1{
            let  alert = UIAlertController(title: NSLocalizedString("Alert_Text", comment: ""), message: NSLocalizedString("Restart_Confirmation_Text", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("Yes_Text", comment: ""), style: UIAlertActionStyle.default, handler: { action in self.restartGame()}))
            alert.addAction(UIAlertAction(title: NSLocalizedString("No_Text", comment: ""), style: UIAlertActionStyle.default, handler: { action in self.startChrono()}))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        if action==2{
            let  alert = UIAlertController(title: NSLocalizedString("Alert_Text", comment: ""), message: NSLocalizedString("Finish_Confirmation_Text", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("Yes_Text", comment: ""), style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
            alert.addAction(UIAlertAction(title: NSLocalizedString("No_Text", comment: ""), style: UIAlertActionStyle.default, handler: { action in self.startChrono()}))
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    func restartGame(){
        self.currentLevel = 1
        self.lifes=2
        self.createPaths()
        self.highScore=0
        self.score=0
        self.gainedScore=0
        chooseOnGamePath()
        self.currentPosition=1
        self.seconds=4
        
        showLifes()
        self.setDiamondPosition(position: self.currentPosition)
        
        self.scoreLabel.text="00"
        self.timeLeft.text=""
        
        self.hasStarted=false
        self.timerImage.isHidden=true
        
        self.menuButton.setBackgroundImage(UIImage(named: "play_button"), for: UIControlState.normal)
        self.menuButton.isEnabled=true
        if Reachability.isConnectedToNetwork() == true{
            if self.interstitial.isReady{
                
                presentAd()
            }
        }
        
    }
    func gameOver(){
        if self.isHighScore(){
            self.setHighScore()
            self.showAlertHighScore()
        }
        else{
            self.unlockButtons()
            var message = "\n\(NSLocalizedString("Current_Score_Text", comment: "")) \(self.score) \n\(NSLocalizedString("Current_HighScore_Text", comment: "")) \(self.getHighScore())"
            let  alert = UIAlertController(title: NSLocalizedString("End_Game_Text", comment: ""), message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Restart_Game_Text", comment: ""), style: UIAlertActionStyle.default, handler: { action in self.restartGame()}))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Finish_Game_Text", comment: ""), style: UIAlertActionStyle.default, handler: { action in self.performSegue(withIdentifier: "goMainMenu", sender: self)}))
        
            self.present(alert, animated: true, completion: nil)
        }

    }
    
    //ANIMATIONS
    
    func animateCurrentPath(index:Int){
        self.lookIcon.isHidden=false
        self.lockButtons()
        var when = DispatchTime.now() + 0.1
        DispatchQueue.main.asyncAfter(deadline: when) {
            if self.selectedPath[index]==1{
                self.animate(item: 1, index: index)
            }
            if self.selectedPath[index]==2{
                self.animate(item: 2, index: index)
            }
            if self.selectedPath[index]==3{
                self.animate(item: 3, index: index)
            }
            if self.selectedPath[index]==4{
                self.animate(item: 4, index: index)
            }
        }
        
    }
    func animate(item:Int, index:Int){
        
        var when = DispatchTime.now() + 0.7
        //progressView.setProgress(1.0, animated: false)
        self.seconds=4
        //self.progressView.progress=1.0
        //self.lockButtons()
        if item==1{
            
            self.hilightButton(key: 1)
            self.makeSound(key: 1)
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.deHilightButton(key: 1)
                //if index+1 < self.selectedPath.count{
                if index+1 < self.currentLevel{
                    self.animateCurrentPath(index: index+1)
                }
                else{
                    //when = DispatchTime.now() + 1.5
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.unlockButtons()
                        self.lookIcon.isHidden=true
                        self.resetChrono()  //RC
                    }
                }
                
            }
            
        }
        if item==2{
            
            self.hilightButton(key: 2)
            self.makeSound(key: 2)
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.deHilightButton(key: 2)
                if index+1 < self.currentLevel{
                    self.animateCurrentPath(index: index+1)
                }
                else{
                    //when = DispatchTime.now() + 1.5
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.unlockButtons()
                        self.lookIcon.isHidden=true
                        self.resetChrono()  //RC

                    }
                }
            }
        }
        if item==3{
            
            self.hilightButton(key: 3)
            self.makeSound(key: 3)
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.deHilightButton(key: 3)
                if index+1 < self.currentLevel{
                    self.animateCurrentPath(index: index+1)
                }
                else{
                    //when = DispatchTime.now() + 1.5
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.unlockButtons()
                        self.lookIcon.isHidden=true
                        self.resetChrono()  //RC

                    }
                }
            }
        }
        if item==4{
            
            self.hilightButton(key: 4)
            self.makeSound(key: 4)
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.deHilightButton(key: 4)
                if index+1 < self.currentLevel{
                    self.animateCurrentPath(index: index+1)
                }
                else{
                    //when = DispatchTime.now() + 1.5
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.unlockButtons()
                        self.lookIcon.isHidden=true
                        self.resetChrono()  //RC

                    }
                }
            }
        }
        
    }
    func makeSound(key:Int){
        
        
        if key == cb1.buttonValue{
            
            do {audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: cb1.sound! ))}
            catch{
                print(error)
            }
            
        }
        if key == cb2.buttonValue{
            do {audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: cb2.sound! ))}
            catch{
                print(error)
            }
            
        }
        if key == cb3.buttonValue{
            do {audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: cb3.sound! ))}
            catch{
                print(error)
            }
        }
        if key == cb4.buttonValue{
            do {audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: cb4.sound! ))}
            catch{
                print(error)
            }
        }
        
        
        audioPlayer.stop()
        
        audioPlayer.play()
        
       
}
    func hilightButton(key:Int){
        print("Entra a hili")
        
        if self.cb1.buttonValue==key{
            self.image1.backgroundColor=self.cb1.sColor
        }
        if self.cb2.buttonValue==key{
            self.image2.backgroundColor=self.cb2.sColor
        }
        if self.cb3.buttonValue==key{
            self.image3.backgroundColor=self.cb3.sColor
        }
        if self.cb4.buttonValue==key{
            self.image4.backgroundColor=self.cb4.sColor
        }
        
    }
    func deHilightButton(key:Int){
        print("Entra a dehili")
        
        if self.cb1.buttonValue==key{
            self.image1.backgroundColor=self.cb1.nColor
        }
        if self.cb2.buttonValue==key{
            self.image2.backgroundColor=self.cb2.nColor
        }
        if self.cb3.buttonValue==key{
            self.image3.backgroundColor=self.cb3.nColor
        }
        if self.cb4.buttonValue==key{
            self.image4.backgroundColor=self.cb4.nColor
        }
    }
    func deHilightAllButtons(){
        self.image1.backgroundColor=self.cb1.nColor
        self.image2.backgroundColor=self.cb2.nColor
        self.image3.backgroundColor=self.cb3.nColor
        self.image4.backgroundColor=self.cb4.nColor
    }
    func makeLoseSound(){
        
    }
    
    //TAP
    func tappedButton()
    {
        if isPathCorrect(){
            getGainedScore()
            showGainedScore()
            if self.isCorrectWholePath(){
                self.timeLeft.text=""
                self.improveCurrentLevel()
                self.cleanOnGamePath()
                self.getMoreLifes()

                
                let when = DispatchTime.now() + 1.5
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.animateCurrentPath(index: 0)
                }

            }
            else{
                self.resetChrono()  //RC
                self.unlockButtons()

            }
            
        }
        else{
            pathNotCorrect()
        }
    }
    
    func pathNotCorrect(){
        self.loseLife()
        
        let when = DispatchTime.now() + 1.0
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.cleanOnGamePath()
            if self.isAlive(){
                self.animateCurrentPath(index: 0)
            }
            else{
                
                ////////
                
                
                if self.isHighScore(){
                    self.setHighScore()
                    self.showAlertHighScore()
                }
                else{
                    self.gameOver()
                }
            }

        }
        
    }
    
    
    //CREATE PATHS
    func createPaths(){
        //self.allPathsArray.append([1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4])
        //self.allPathsArray.append([1,1,3,3,1,3,1,1,2,2,2,3,3,1,2,4,1,1,2,3,1,2,3,4,1])
        //self.allPathsArray.append([3,4,3,4,4,1,2,4,3,3,4,1,4,1,2,4,1,2,4,4,1,2,4,2,2])
        //self.allPathsArray.append([2,2,4,4,1,1,3,3,1,2,4,1,4,2,1,1,4,4,2,2,3,4,1,2,1])
        //self.allPathsArray.append([1,1,4,4,1,1,4,2,2,3,2,2,3,3,1,3,1,3,4,3,4,1,2,3,4])
        //self.allPathsArray.append([4,3,2,1,4,1,3,2,1,4,2,3,1,2,3,4,1,1,2,2,4,4,1,2,1])
        self.allPathsArray.removeAll()
        self.allPathsArray.append(makePath())
        print("Arreglo creado \(self.allPathsArray[0])")
    }
    func chooseOnGamePath(){
        self.selectedPath=allPathsArray[0]
        //self.selectedPath=allPathsArray[self.randomNumber(MIN: 0, MAX: 4)]
    }
    func makePath()->[Int]{
        var path = [Int]()
        var newItem=0
        
        for i in 0...(39) {
            newItem=randomNumber(MIN: 0, MAX: 100)
            
            if i==2{
                while (path[i-2]==newItem && path[i-1]==newItem){
                    newItem=randomNumber(MIN: 0, MAX: 100)
                }
            }
            if i>=3{
                while (path[i-2]==newItem || path[i-3]==newItem){
                    newItem=randomNumber(MIN: 0, MAX: 100)
                }
            }
            path.append(newItem)
        }
        return path
        
        
    }
    
    func randomNumber(MIN: Int, MAX: Int)-> Int{
        var number = Int(arc4random_uniform(UInt32(MAX)) + UInt32(MIN))
        var returnNumber=0
        if number>=0&&number<=24{
            returnNumber=1
        }
        if number>=25&&number<=49{
            returnNumber=2
        }
        if number>=50&&number<=74{
            returnNumber=3
        }
        if number>=75&&number<=100{
            returnNumber=4
        }
        return returnNumber
        
    }
    
    //SCORE
    
    func getGainedScore(){
        self.gainedScore = (Int(5.0 * (self.seconds)))
        
    }
    func getUpdatedScore(){
        self.score = self.score + self.gainedScore
    }
    func showGainedScore(){
        self.gainPointsLabel.text="+ \(String(self.gainedScore))"
        
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.gainPointsLabel.text=""
            self.getUpdatedScore()
            self.showScore()
        }
    }
    func showScore(){
        self.scoreLabel.text = String(self.score)
    }
    func showLifes(){
        if self.lifes>=0{
            self.lifesLabel.text = "\(NSLocalizedString("Lifes_Text", comment: "")) \(String(self.lifes))"
        }
        
    }
    func setTimerImage(){
        if self.seconds==0{
            self.timerImage.image=UIImage(named: "timer_0.png")
            
            
            let bounds = self.timerImage.bounds
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                self.timerImage.bounds = CGRect(x: bounds.origin.x - 30, y: bounds.origin.y, width: bounds.size.width + 5, height: bounds.size.height)
            }, completion: nil)
        }
        if self.seconds==1{
            self.timerImage.image=UIImage(named: "timer_1.png")
        }
        if self.seconds==2{
            self.timerImage.image=UIImage(named: "timer_2.png")
        }
        if self.seconds==3{
            self.timerImage.image=UIImage(named: "timer_3.png")
            
        }
        if self.seconds==4{
            self.timerImage.image=UIImage(named: "timer_4.png")
        }
        
    }
    
    func setDiamondPosition(position:Int){
        self.controlView.rotate360Degrees()
                let when = DispatchTime.now() + 0.8
        DispatchQueue.main.asyncAfter(deadline: when) {
            if position==1{
                self.cb1.setValue(index: 1)
                self.cb2.setValue(index: 2)
                self.cb3.setValue(index: 3)
                self.cb4.setValue(index: 4)
            }
            if position==2{
                self.cb1.setValue(index: 4)
                self.cb2.setValue(index: 1)
                self.cb3.setValue(index: 2)
                self.cb4.setValue(index: 3)
            }
            if position==3{
                self.cb1.setValue(index: 3)
                self.cb2.setValue(index: 4)
                self.cb3.setValue(index: 1)
                self.cb4.setValue(index: 2)
            }
            if position==4{
                self.cb1.setValue(index: 2)
                self.cb2.setValue(index: 3)
                self.cb3.setValue(index: 4)
                self.cb4.setValue(index: 1)
            }

            self.image1.backgroundColor=self.cb1.nColor
            self.image2.backgroundColor=self.cb2.nColor
            self.image3.backgroundColor=self.cb3.nColor
            self.image4.backgroundColor=self.cb4.nColor}
        
    }
    
    func selectDiamonPosition(){
        /*var newPosition = self.randomNumber(MIN: 1, MAX: 4)
        while self.curentPosition == newPosition{
            newPosition = self.randomNumber(MIN: 1, MAX: 4)
        }*/
        var newPosition = self.currentPosition+1
        if newPosition==5{newPosition=1}
        self.currentPosition=newPosition
        setDiamondPosition(position: self.currentPosition)
        
    }
    
    func loseLifeAnimation(){
        /*let bounds = self.controlView.bounds
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.controlView.bounds = CGRect(x: bounds.origin.x - 30, y: bounds.origin.y, width: bounds.size.width + 10, height: bounds.size.height)
        }, completion: nil)*/
        
        let bounds = self.controlView.bounds
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.controlView.bounds = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width + 40, height: bounds.size.height)
        }, completion: nil)
        controlView.center.x=self.view.center.x
        
        //PLaylose sound
        var sound = Bundle.main.path(forResource: "lose", ofType: "mp3")
        do {audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound! ))}
        catch{
            print(error)
        }
        audioPlayer.stop()
        
        audioPlayer.play()
        
        //haptic functions 7 & 7+
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.error)
    }
    
    
    //INITIAL ANIMATION
    
    var initialAnimationArray = [1,2,3,4,1,2,3,4]
    func initialPresentation(index:Int){
        self.lockButtons()
        /*var when = DispatchTime.now() + 0.1
        if index<=7{
            DispatchQueue.main.asyncAfter(deadline: when) {
                if self.initialAnimationArray[index]==1{
                    self.animateInitial(item: 1, index: index)
                }
                if self.initialAnimationArray[index]==2{
                    self.animateInitial(item: 2, index: index)
                }
                if self.initialAnimationArray[index]==3{
                    self.animateInitial(item: 3, index: index)
                }
                if self.initialAnimationArray[index]==4{
                    self.animateInitial(item: 4, index: index)
                }
            }
        }
        else{
            var when = DispatchTime.now() + 1.0
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.animateCurrentPath(index: 0)
            }
        }*/
        self.animateCurrentPath(index: 0)
        
        
        
    }

    func animateInitial(item:Int, index:Int){
        
        var when = DispatchTime.now() + 0.2
    
        if item==1{
            
            self.hilightButton(key: 1)
            self.makeSound(key: 1)
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.deHilightButton(key: 1)
                
                if index+1 <= 7{
                    self.initialPresentation(index: index+1)
                }
                else{
                    //when = DispatchTime.now() + 1.5
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.unlockButtons()
                    }
                }
                
            }
            
        }
        if item==2{
            
            self.hilightButton(key: 2)
            self.makeSound(key: 2)
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.deHilightButton(key: 2)
                if index+1 <= 8{
                    self.initialPresentation(index: index+1)
                }
                else{
                    //when = DispatchTime.now() + 1.5
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.unlockButtons()
                    }
                }
            }
        }
        if item==3{
            
            self.hilightButton(key: 3)
            self.makeSound(key: 3)
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.deHilightButton(key: 3)
                if index+1 <= 8{
                    self.initialPresentation(index: index+1)
                }
                else{
                    //when = DispatchTime.now() + 1.5
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.unlockButtons()
                    }
                }
            }
        }
        if item==4{
            
            self.hilightButton(key: 4)
            self.makeSound(key: 4)
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.deHilightButton(key: 4)
                if index+1 <= 8{
                    self.initialPresentation(index: index+1)
                }
                else{
                    //when = DispatchTime.now() + 1.5
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.unlockButtons()
                    }
                }
            }
        }
        
    }
    
    //-----Ads
    func createAndLoadAd() -> GADInterstitial
    {
        var ad = GADInterstitial(adUnitID: "ca-app-pub-1462291695436824/8430183934")
        
        
        var request = GADRequest()
        
        request.testDevices = ["2077ef9a63d2b398840261c8221a0c9b"]
        
        ad.load(request)
        return ad
        
        /*var interstitial = GADInterstitial(adUnitID: "ca-app-pub-4708498702836503/8147076471")
         interstitial.delegate = self
         interstitial.load(GADRequest())
         return interstitial*/
        
    }
    func interstitialDidDismissScreen(ad: GADInterstitial!) {
        
        if Reachability.isConnectedToNetwork() == true
        {
            interstitial = createAndLoadAd()
        }
    }
    
    
    func presentAd(){
        var x = 0
        while x==0{
            if (self.interstitial.isReady)
            {
                self.interstitial.present(fromRootViewController: self)
                if Reachability.isConnectedToNetwork() == true
                {
                    interstitial = createAndLoadAd()
                }
                self.interstitial = self.createAndLoadAd()
                x=1
            }
        }
        
    }
    
    //--------
    
    
    
    
}

