//
//  customButton.swift
//  tawa
//
//  Created by bot on 8/23/17.
//  Copyright © 2017 Cristopher Nunez Del Prado. All rights reserved.
//

import Foundation
import UIKit

class customButton{
    var buttonValue = 0
    var sColor = UIColor(colorLiteralRed: 247, green: 102, blue: 102, alpha: 1.0)
    var nColor = UIColor(colorLiteralRed: 207, green: 0, blue: 21, alpha: 1.0)
    var sound = Bundle.main.path(forResource: "S1", ofType: "mp3")
    
    init(){
        
    }
    func setValue(index:Int){
        if index == 1{
            buttonValue=1
            nColor = UIColor(colorLiteralRed: 217/255.0, green: 25/255.0, blue: 38/255.0, alpha: 1.0)
            sColor = UIColor(colorLiteralRed: 249/255.0, green: 126/255.0, blue: 124/255.0, alpha: 1.0)
            sound = Bundle.main.path(forResource: "S1", ofType: "mp3")
        }
        if index == 2{
            buttonValue=2
            nColor = UIColor(colorLiteralRed: 28/255.0, green: 123/255.0, blue: 36/255.0, alpha: 1.0)
            sColor = UIColor(colorLiteralRed: 102/255.0, green: 255/255.0, blue: 102/255.0, alpha: 1.0)
            sound = Bundle.main.path(forResource: "S2", ofType: "mp3")
        }
        if index == 3{
            buttonValue=3
            nColor = UIColor(colorLiteralRed: 14/255.0, green: 49/255.0, blue: 159/255.0, alpha: 1.0)
            sColor = UIColor(colorLiteralRed: 102/255.0, green: 205/255.0, blue: 255/255.0, alpha: 1.0)
            sound = Bundle.main.path(forResource: "S3", ofType: "mp3")
        }
        if index == 4{
            buttonValue=4
            nColor = UIColor(colorLiteralRed: 234/255.0, green: 171/255.0, blue: 17/255.0, alpha: 1.0)
            sColor = UIColor(colorLiteralRed: 255/255.0, green: 255/255.0, blue: 11/255.0, alpha: 1.0)
            sound = Bundle.main.path(forResource: "S4", ofType: "mp3")
            
        }
    }
}
