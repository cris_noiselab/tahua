//
//  presentation.swift
//  tawa
//
//  Created by Cristopher Nunez Del Prado on 25/08/17.
//  Copyright © 2017 Cristopher Nunez Del Prado. All rights reserved.
//


import Foundation
import UIKit
import GameKit


class presentation: UIViewController {
    
    @IBOutlet var titleIcon: UIImageView!
    @IBOutlet var firstLogo: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        animation()
        let aSelector : Selector = "timeToMoveOn"
        let timer = Timer.scheduledTimer(timeInterval: 4.3, target: self, selector: aSelector, userInfo: nil, repeats: false)
    }
    func timeToMoveOn() {
        self.performSegue(withIdentifier: "goMainMenu", sender: self)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func animation(){
        
        var when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.blink(sw: 1,cont: 0)
        }
        when = DispatchTime.now() + 2.0
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.blink(sw: 1,cont: 0)
        }
        
        
        /*let bounds = self.titleIcon.bounds
         UIView.animate(withDuration: 3.0, delay: 0.5, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: UIViewAnimationOptions.curveLinear, animations: {
         self.titleIcon.bounds = CGRect(x: bounds.origin.x - 20, y: bounds.origin.y, width: bounds.size.width + 60, height: bounds.size.height)
         
         }, completion: nil)*/
        
    }
    
    func blink(sw:Int, cont:Int){
        if cont<3{
            let when = DispatchTime.now() + 0.5
            
            if sw==1{
                self.firstLogo.image=UIImage.init(named: "bitroomicon1.png")
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.blink(sw: 2,cont: cont+1)
                }
            }else{
                self.firstLogo.image=UIImage.init(named: "bitroomicon2.png")
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.blink(sw: 1,cont: cont+1)
                }
            }
            
            
            
            
        }
        
    }
    
    
    
    
}



